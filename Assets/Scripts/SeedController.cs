﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedController : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    public Sprite SeedImage;
    public Sprite GrowingImage;
    public Sprite DoneImage;

    private SEED_STATUS previousStatus;
    public SEED_STATUS currentStatus;
    private float growthTime = 5f;

    // Start is called before the first frame update
    void Start()
    {
        UpdateSprite();
    }

    // Update is called once per frame
    void Update()
    {
        if(previousStatus != currentStatus)
        {
            UpdateSprite();
            previousStatus = currentStatus;
        }
        if (currentStatus == SEED_STATUS.IN_PROGRESS)
            growthTime -= Time.deltaTime;
        if (growthTime < 0)
        {
            currentStatus = SEED_STATUS.DONE;
            growthTime = 5f;
        }
            
    }

    private void UpdateSprite()
    {
        switch(currentStatus)
        {
            case SEED_STATUS.NONE:
                spriteRenderer.sprite = null;
                break;
            case SEED_STATUS.SEED:
                spriteRenderer.sprite = SeedImage;
                break;
            case SEED_STATUS.IN_PROGRESS:
                spriteRenderer.sprite = GrowingImage;
                break;
            case SEED_STATUS.DONE:
                spriteRenderer.sprite = DoneImage;
                break;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController playerController = collision.gameObject.GetComponent<PlayerController>();
        if(playerController)
        {
            Debug.Log(string.Format("Set target to:{0}", gameObject.name));
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController playerController = collision.gameObject.GetComponent<PlayerController>();
        if (playerController)
        {            
            Debug.Log(string.Format("Set target to: NONE"));
        }
    }

    public void SetSeedStatus(SEED_STATUS status)
    {
        currentStatus = status;
    }

    public SEED_STATUS GetSeedStatus()
    {
        return currentStatus;
    }
}
