﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// TODO (lac): This singleton is a "hack" to access everything from this class
//  We should refactor the code to remove all singletons
public class GameEngineSingleton: MonoBehaviour
{
    private static GameEngineSingleton instance;
    public static GameEngineSingleton Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameEngineSingleton>();
                instance.InitializeValues();
            }
            return instance;
        }
    }

    public TOOL_TYPE CurrentToolType;

    public Dictionary<int, GameObject> SeedSlots;
    public Dictionary<int, GameObject> GrassHoppers;
    public Dictionary<int, GameObject> GameElement;

    public Image Health1;
    public Image Health2;
    public Image Health3;

    public GameObject GameOverPanel;
    public GameObject GrasshopperSpawner;
    public GameObject Player;
    // TODO (lac): This is a hack to fake a grid when serializing x/y coordinates  (kinda like a hash). As we have less than 100 rows, it wont be a problem :)
    private int _gridRows = 100;

    public AudioClip WateringAudio;
    public AudioClip ToolAudio;
    public AudioClip SquishAudio;
    public AudioClip ItemEquipAudio;
    public AudioClip PunchAudio;
    public AudioClip InsectAudio;
    public AudioClip ScytheAudio;
    private AudioSource _audioSource;

    public Text ScoreText;
    public Text InsectsText;

    private int Score = 0;
    private int Squish = 0;
    private int Health = 3;


    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void InitializeValues()
    {
        instance.Score = 0;
        instance.Squish = 0;
        instance.Health = 3;

        instance.SeedSlots = new Dictionary<int, GameObject>();
        instance.GrassHoppers = new Dictionary<int, GameObject>();
        instance.GameElement = new Dictionary<int, GameObject>();
    }

    public void AddSeedSlots(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        SeedSlots.Add(index, go);
    }

    public GameObject GetSeedSlot(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        if (SeedSlots.ContainsKey(index))
        {
            GameObject seedSlot = SeedSlots[index];
            return seedSlot;
        }
        return null;
    }

    public GameObject GetRandomSeedSlot()
    {
        int totalElements = SeedSlots.Count;
        if (totalElements == 0)
            return null;

        // NB (lac): get a random element from a dict
        int index = UnityEngine.Random.Range(0, totalElements);
        var e = SeedSlots.GetEnumerator();
        for (int i = 0; i < index; i++)
        {
            e.MoveNext();
        }
        GameObject seedSlot = e.Current.Value;
        return seedSlot;
    }

    public void PlayScytheAudio()
    {
        _audioSource.clip = ScytheAudio;
        _audioSource.Play();
    }
    public void PlayInsectAudio()
    {
        _audioSource.clip = InsectAudio;
        _audioSource.Play();
    }
    public void PlayPunchAudio()
    {
        _audioSource.clip = PunchAudio;
        _audioSource.Play();
    }
    public void PlayItemEquipAudio()
    {
        _audioSource.clip = ItemEquipAudio;
        _audioSource.Play();
    }
    public void PlayWateringAudio()
    {
        // NB (lac): play hit sound
        _audioSource.clip = WateringAudio;
        _audioSource.Play();
    }

    public void PlayToolAudio()
    {
        // NB (lac): play hit sound
        _audioSource.clip = ToolAudio;
        _audioSource.Play();
    }

    public void PlaySquishAudio()
    {
        // NB (lac): play hit sound
        _audioSource.clip = SquishAudio;
        _audioSource.Play();
    }

    public void AddGameElement(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        GameElement.Add(index, go);
    }

    public bool RemoveGameElement(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        if (GameElement.ContainsKey(index))
        {
            GameObject gElement = GameElement[index];
            Destroy(gElement.gameObject);
            GameElement.Remove(index);
            return true;
        }
        return false;
    }

    public GameObject GetGameElement(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        if (GameElement.ContainsKey(index))
        {
            GameObject gElement = GameElement[index];
            return gElement;
        }
        return null;
    }

    public GameObject GetGameElementByPosition(Vector3 position)
    {
        int index = (int)(Math.Floor(position.x) * _gridRows + Math.Floor(position.y));
        if (GameElement.ContainsKey(index))
        {
            GameObject gElement = GameElement[index];
            return gElement;
        }
        return null;
    }

    public void AddGrassHopper(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        GrassHoppers.Add(index, go);
    }

    public GameObject GetGrassHopper(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        if (GrassHoppers.ContainsKey(index))
        {
            GameObject grasshopper = GrassHoppers[index];
            return grasshopper;
        }
        return null;
    }

    public void RemoveGrassHopper(GameObject go)
    {
        int index = (int)(Math.Floor(go.transform.position.x) * _gridRows + Math.Floor(go.transform.position.y));
        if (GrassHoppers.ContainsKey(index))
        {
            GameObject grasshopper = GrassHoppers[index];
            Destroy(grasshopper.gameObject);
            GrassHoppers.Remove(index);
            PlaySquishAudio();
            GameEngineSingleton.Instance.AddSqhish();
        }
    }
    
    
    public void AddScore()
    {
        Score++;
        ScoreText.text = String.Format("Vegetables:{0}", Score);
    }

    public void AddSqhish()
    {
        Squish++;
        InsectsText.text = String.Format("Insects:{0}", Squish);
    }

    public void DecreaseHealth()
    {        
        Health--;
        switch(Health)
        {
            case 2:
                Health3.color = new Color(Health3.color.r, Health3.color.g, Health3.color.b, 0);
                break;
            case 1:
                Health2.color = new Color(Health2.color.r, Health2.color.g, Health2.color.b, 0);
                break;
            case 0:
                Health1.color = new Color(Health1.color.r, Health1.color.g, Health1.color.b, 0);
                GameOverPanel.SetActive(true);
                GrasshopperSpawner.SetActive(false);
                break;
            default:
                GameOverPanel.SetActive(true);
                GrasshopperSpawner.SetActive(false);
                break;
        }
        GameEngineSingleton.Instance.PlayPunchAudio();
    }

    public void Restart()
    {
        InitializeValues();
        SceneManager.LoadScene("SampleScene");
    }

    public GameObject GetPlayerObject()
    {
        return Player;
    }
}
