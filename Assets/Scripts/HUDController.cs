﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    public Image ToolImage;

    public Sprite SeedBagSprite;
    public Sprite WateringCanSprite;
    public Sprite HoeSprite;
    public Sprite ScytheSprite;
    public Sprite SpraySprite;

    private TOOL_TYPE previousToolType;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // Check for tool update
        if(previousToolType != GameEngineSingleton.Instance.CurrentToolType)
        {
            previousToolType = GameEngineSingleton.Instance.CurrentToolType;
            switch(previousToolType)
            {
                case TOOL_TYPE.SEEDBAG:
                    ToolImage.sprite = SeedBagSprite;
                    ToolImage.color = new Color32(255, 255, 255, 255);
                    break;
                case TOOL_TYPE.WATERINGCAN:
                    ToolImage.sprite = WateringCanSprite;
                    ToolImage.color = new Color32(255, 255, 255, 255);
                    break;
                case TOOL_TYPE.HOE:
                    ToolImage.sprite = HoeSprite;
                    ToolImage.color = new Color32(255, 255, 255, 255);
                    break;
                case TOOL_TYPE.SCYTHE:
                    ToolImage.sprite = ScytheSprite;
                    ToolImage.color = new Color32(255, 255, 255, 255);
                    break;
                case TOOL_TYPE.SPRAY:
                    ToolImage.sprite = SpraySprite;
                    ToolImage.color = new Color32(255, 255, 255, 255);
                    break;
            }
            GameEngineSingleton.Instance.PlayItemEquipAudio();
        }
    }
}
