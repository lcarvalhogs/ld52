﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAssetsSpawnerController : MonoBehaviour
{
    public GameObject RockBig;

    [SerializeField]
    public List<SpawnRegion> Regions;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < Regions.Count; i++)
        {
            if(Regions[i].TotalElements > 0)
            {
                int totalElementsToSpawn = Random.Range(1, Regions[i].TotalElements);
                Debug.Log(string.Format("Spawning {0} elements for region {1}", totalElementsToSpawn, Regions[i].gameObject.name));
                // NB (lac): for each element
                for (int elementIndex = 0; elementIndex < totalElementsToSpawn; elementIndex++)
                {
                    Vector3 startingPosition = Regions[i].transform.position;                    
                    startingPosition.x += Random.Range(0, Regions[i].Radius-1);
                    startingPosition.y += Random.Range(0, Regions[i].Radius-1);

                    startingPosition.x = (float)System.Math.Floor(startingPosition.x) + .5f;
                    startingPosition.y = (float)System.Math.Floor(startingPosition.y) + .5f;
                    Debug.Log(string.Format("{0}:{1}", Regions[i].gameObject.name, startingPosition));
                    GameObject go = GameEngineSingleton.Instance.GetGameElementByPosition(startingPosition);
                    if(!go)
                    {
                        go = Instantiate(RockBig, startingPosition, Quaternion.identity);                        
                        GameEngineSingleton.Instance.AddGameElement(go);
                    }
                    else
                    {
                        Debug.Log(string.Format("Object already exists: ignoring elemet!", totalElementsToSpawn, Regions[i].gameObject.name));
                    }
                    
                }
            }
            else
            {
                Debug.Log(string.Format("Noelements to spawn for region {0}", Regions[i].gameObject.name));
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
