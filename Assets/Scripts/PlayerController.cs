﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Movement speed
    public float moveSpeed = 5.0f;

    public Animator animatorController;
    private DIRECTION _currentDirection;

    public GameObject SeedSlotPrefab;

    public GameObject _actionArea;

    // Update is called once per frame
    void Update()
    {
        UpdateMovement();
        UpdateAction();
        UpdateActionArea();
    }

    private void UpdateMovement()
    {
        // Get input for movement
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calculate movement
        Vector3 moveDirection = new Vector3(horizontalInput, verticalInput, 0);
        transform.position += moveDirection * moveSpeed * Time.deltaTime;

        animatorController.SetFloat("MoveX", horizontalInput);
        animatorController.SetFloat("MoveY", verticalInput);

        if(horizontalInput > 0)
        {
            _currentDirection = DIRECTION.RIGHT;
            
        }
        else if (horizontalInput < 0)
        {
            _currentDirection = DIRECTION.LEFT;
        }
        else if (verticalInput > 0)
        {
            _currentDirection = DIRECTION.UP;
        }
        else if (verticalInput < 0)
        {
            _currentDirection = DIRECTION.DOWN;
        }
    }

    private void UpdateAction()
    {
        if (Input.GetButtonDown("Action"))
        {
            Debug.Log("Action button was pressed!");
            TOOL_TYPE currentAction = GameEngineSingleton.Instance.CurrentToolType;            
            DoAction(currentAction);
        }
    }

    private Vector3 GetCurrentTilePosition()
    {
        // Each unit stands for 16 pixels
        return new Vector3((float)Math.Truncate(transform.position.x)+.5f, (float)Math.Truncate(transform.position.y) + .5f, -1);
    }

    private Vector3 GetActionPosition()
    {
        return _actionArea.transform.position;
    }

    private Vector3 GetTileAhead()
    {
        float offsetValue = .0f;
        Vector3 offsetVector = new Vector3(0, 0, 0);
        switch (_currentDirection)
        {
            case DIRECTION.LEFT:
                offsetVector.x -= offsetValue;
                break;
            case DIRECTION.RIGHT:
                offsetVector.x += offsetValue + .1f;
                break;
            case DIRECTION.UP:
                offsetVector.y += offsetValue + .1f; ;
                break;
            case DIRECTION.DOWN:
                offsetVector.y -= offsetValue;
                break;

        }
        if(offsetVector.x > 0)
        {
            offsetVector.x = (float)Math.Ceiling(transform.position.x + offsetVector.x);
        }
        else
        {
            offsetVector.x = (float)Math.Floor(transform.position.x + offsetVector.x);
        }

        if (offsetVector.y > 0)
        {
            offsetVector.y = (float)Math.Ceiling(transform.position.y + offsetVector.y);
        }
        else
        {
            offsetVector.y = (float)Math.Floor(transform.position.y + offsetVector.y);
        }
        return new Vector3(offsetVector.x, offsetVector.y, -2);
    }

    private void UpdateActionArea()
    {
        Vector3 newLocation = GetCurrentTilePosition();
        newLocation.z = -2;
        _actionArea.transform.position = newLocation;
    }

    // NB (lac): update HUDController script when adding a new case to this method!
    private void DoAction(TOOL_TYPE action)
    {
        if(_actionArea)
        {
            Vector3 actionPosition = GetActionPosition();
            GameObject seedSlot = GameEngineSingleton.Instance.GetSeedSlot(_actionArea);
            GameObject insect = GameEngineSingleton.Instance.GetGrassHopper(_actionArea);
            GameObject gameElement = GameEngineSingleton.Instance.GetGameElement(_actionArea);
            SeedController seedScript = null;
            if(seedSlot)
            {
                seedScript = seedSlot.GetComponent<SeedController>();
            }
            switch (action)
            {
                case TOOL_TYPE.SEEDBAG:
                    if (seedScript 
                        && seedScript.GetSeedStatus() == SEED_STATUS.NONE
                        && !insect)
                    {
                        Debug.Log(String.Format("Using SeedBag! Tile Position: {0}", actionPosition));
                        GameEngineSingleton.Instance.PlayToolAudio();
                        seedScript.SetSeedStatus(SEED_STATUS.SEED);
                    }
                    break;
                case TOOL_TYPE.WATERINGCAN:
                    Debug.Log(String.Format("Using WateringCan! Tile Position: {0}", actionPosition));
                    if (seedScript
                        && seedScript.GetSeedStatus() == SEED_STATUS.SEED
                        && !insect)
                    {
                        GameEngineSingleton.Instance.PlayWateringAudio();
                        seedScript.SetSeedStatus(SEED_STATUS.IN_PROGRESS);
                    }
                    break;
                case TOOL_TYPE.HOE:
                    if(!seedSlot
                        && !gameElement)
                    {
                        Debug.Log(String.Format("Using Hoe! Tile Position: {0}", actionPosition));
                        GameEngineSingleton.Instance.PlayToolAudio();
                        if(actionPosition.y < -3)
                        {
                            actionPosition.z = -1;
                            GameObject slot = Instantiate(SeedSlotPrefab, actionPosition, Quaternion.identity);
                            GameEngineSingleton.Instance.AddSeedSlots(slot);
                        }
                        else
                        {
                            Debug.Log(String.Format("Hoe out of bounds! Position: {0}", actionPosition));
                        }
                        
                    }
                    else
                    {
                        Debug.Log(String.Format("Skipping Hoe! Tile Position: {0}", actionPosition));
                    }
                    
                    break;
                case TOOL_TYPE.SCYTHE:
                    if (seedScript
                        && seedScript.GetSeedStatus() == SEED_STATUS.DONE
                        && !insect)
                    {
                        Debug.Log(String.Format("Using Scythe! Tile Position: {0}", actionPosition));
                        GameEngineSingleton.Instance.PlayToolAudio();
                        seedScript.SetSeedStatus(SEED_STATUS.NONE);
                        GameEngineSingleton.Instance.AddScore();
                        GameEngineSingleton.Instance.PlayScytheAudio();
                    }
                    break;
                case TOOL_TYPE.SPRAY:                    
                    if (insect)
                    {
                        Debug.Log(String.Format("Using Spray! Tile Position: {0}", actionPosition));
                        GameEngineSingleton.Instance.RemoveGrassHopper(_actionArea);
                    }                    
                    break;
                default:
                    break;
            }
        }
    }
}
