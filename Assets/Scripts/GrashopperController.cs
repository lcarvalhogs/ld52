﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrashopperController : MonoBehaviour
{
    public Animator animatorController;
    GameObject target;
    private float _countDown = 15f;
    // Start is called before the first frame update
    void Start()
    {
        target = GameEngineSingleton.Instance.GetRandomSeedSlot();
    }

    // Update is called once per frame
    void Update()
    {
        _countDown -= Time.deltaTime;
        if (_countDown < 0)
        {
            GameEngineSingleton.Instance.DecreaseHealth();
            _countDown = 15f;
        }
            
    }

    public void Death()
    {
        Debug.Log("LOL");
        //Destroy(gameObject);
    }
}
