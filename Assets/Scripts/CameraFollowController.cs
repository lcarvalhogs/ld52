﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowController : MonoBehaviour
{
    // Target to follow
    public Transform target;

    // Offset from the target
    public Vector3 offset;

    // Smooth factor for camera movement
    public float smoothFactor = 0.125f;

    // Bounds for camera movement
    public float leftBound;
    public float rightBound;
    public float topBound;
    public float bottomBound;

    // Update is called once per frame
    void Update()
    {
        // Calculate desired position
        Vector3 desiredPosition = target.position + offset;

        // Clamp desired position within bounds
        desiredPosition.x = Mathf.Clamp(desiredPosition.x, leftBound, rightBound);
        desiredPosition.y = Mathf.Clamp(desiredPosition.y, topBound, bottomBound);

        // Smoothly move the camera towards the desired position
        transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothFactor);
    }
}
