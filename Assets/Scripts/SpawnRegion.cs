﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts
{
    [SerializeField]
    public class SpawnRegion: MonoBehaviour
    {
        public int TotalElements;
        public float Radius;

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, Radius);
        }
    }
}
