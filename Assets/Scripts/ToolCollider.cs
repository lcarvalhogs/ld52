﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolCollider : MonoBehaviour
{
    public TOOL_TYPE ToolType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameEngineSingleton.Instance.CurrentToolType = ToolType;
        Debug.Log(ToolType);
    }
}
