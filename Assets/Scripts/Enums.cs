﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum DIRECTION
    {
        NONE,
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    public enum TOOL_TYPE
    {
        NONE,
        SEEDBAG,
        WATERINGCAN,
        HOE,
        SCYTHE,
        SPRAY,
    }

    public enum SEED_STATUS
    {
        NONE,
        SEED,
        IN_PROGRESS,
        DONE
    }
}
