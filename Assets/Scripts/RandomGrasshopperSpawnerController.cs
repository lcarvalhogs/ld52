﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGrasshopperSpawnerController : MonoBehaviour
{
    public GameObject GrasshopperPrefab;

    public float SpawnTime = 3f;
    private float _countDown;
    public float Radius = 5f;
    // Start is called before the first frame update
    void Start()
    {
        _countDown = SpawnTime;
    }

    // Update is called once per frame
    void Update()
    {
        _countDown -= Time.deltaTime;
        GameObject gh;
        if (_countDown < 0)
        {
            Vector3 spawnPosition = gameObject.transform.position;
            spawnPosition.x += UnityEngine.Random.Range(0, Radius - 1);
            spawnPosition.y += UnityEngine.Random.Range(0, Radius - 1);
            _countDown = SpawnTime;
            GameObject targetSeed = GameEngineSingleton.Instance.GetRandomSeedSlot();
            if(targetSeed)
            {
                spawnPosition.x = targetSeed.transform.position.x;
                spawnPosition.y = targetSeed.transform.position.y;
                spawnPosition.z = GrasshopperPrefab.transform.position.z;

                gh = GameEngineSingleton.Instance.GetGrassHopper(targetSeed);
                if(!gh)
                {
                    Debug.Log(String.Format("Rock at: {0}:{1}:{2}", spawnPosition.x, spawnPosition.y, spawnPosition.z));

                    // TODO (lac): Wrap this into a function, so that it can also be used by SpawnGrasshopperNearbyPlayer 
                    gh = Instantiate(GrasshopperPrefab, spawnPosition, Quaternion.identity);
                    GameEngineSingleton.Instance.PlayInsectAudio();
                }
                else
                {
                    Debug.Log(string.Format("Grasshopper already exists at {0}:{1}", gh.transform.position.x, gh.transform.position.x));
                    gh = SpawnGrasshopperNearbyPlayer();
                }

            }
            else
            {
                Debug.Log("No seed placed.");
                gh = SpawnGrasshopperNearbyPlayer();                
            }

            if (gh)
                GameEngineSingleton.Instance.AddGrassHopper(gh);
        }

    }

    private GameObject SpawnGrasshopperNearbyPlayer()
    {
        Vector3 ghPosition = GameEngineSingleton.Instance.GetPlayerObject().transform.position;
        ghPosition.x = (float)Math.Floor(ghPosition.x + UnityEngine.Random.Range(1, 3)) + .5f;
        ghPosition.y = (float)Math.Floor(ghPosition.y + UnityEngine.Random.Range(1, 3)) + .5f;

        // TODO (lac): This fixes spawning elements out of player reach. Note we only deal with one side of each axys, as the ramdom range is always positive.
        if (ghPosition.x > 9)
            ghPosition.x -= 4;
        if (ghPosition.y > -3)
            ghPosition.y -= 4;
        //if(ghPosition.y)
        GameObject gh = Instantiate(GrasshopperPrefab, ghPosition, Quaternion.identity);
        GameEngineSingleton.Instance.PlayInsectAudio();
        return gh;
    }
}
